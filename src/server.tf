resource discord_server pparadise {
    name = "Pixel Paradise"
    region = "us-east"
    afk_channel_id = "615638001858379973"
    afk_timeout = 3600
    default_message_notifications = 1
    explicit_content_filter = 2
    verification_level = 2
}
