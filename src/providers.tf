terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/50231155/terraform/state/default"
    lock_address = "https://gitlab.com/api/v4/projects/50231155/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/50231155/terraform/state/default/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_max = 5
  }
  required_providers {
    discord = {
      source = "Lucky3028/discord"
      version = "1.5.0"
    }
  }
}

variable "token" {
  type = string
  sensitive = true
  description = "Discord Bot Token"
  
}

provider "discord" {
    token = var.token
}